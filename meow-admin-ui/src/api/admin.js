import request from "@/utils/request";

// 获取话题列表
export function saveInfo(params = {}) {
    return request({
        url: "admin/save",
        method: "post",
        data: params,
    });
}