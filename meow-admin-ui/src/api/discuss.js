import request from "@/utils/request";

// 获取话题列表
export function discussList(params = {}) {
  return request({
    url: "discuss/getListByPage",
    method: "post",
    data:params,
  });
}

// 删除话题
export function discussDelete(params = {}) {
  return request({
    url: "discuss/delete",
    method: "post",
    data: params,
  });
}
