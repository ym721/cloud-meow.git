import request from '@/utils/request'

// 获取帖子列表
export function linkList(params = {}) {
    return request({
        url: 'link/list',
        method: 'get',
        params
    });
}