import request from "@/utils/request";

// 获取帖子列表
export function postList(params = {}) {
  return request({
    url: "post/getListByPage",
    method: "post",
    data:params,
  });
}

// 删除帖子
export function postDelete(params = {}) {
  return request({
    url: "post/delete",
    method: "post",
    data: params
  });
}
