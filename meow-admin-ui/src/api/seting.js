import request from '@/utils/request'

// 获取系统配置信息
export function getSystemInfo(params = {}) {
    return request({
        url: 'config/info',
        method: 'get', 
        params
    });
}

// 保存系统配置信息
export function systemSave(params = {}) {
    return request({
        url: 'config/save',
        method: 'post', 
        data:params
    });
}