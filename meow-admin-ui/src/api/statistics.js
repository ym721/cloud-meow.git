import request from '@/utils/request'

// 用户统计
export function userStatistics(params = {}) {
    return request({
        url: 'statistics/user',
        method: 'post',
        data: params
    });
}

// 帖子统计
export function postStatistics(params = {}) {
    return request({
        url: 'statistics/post',
        method: 'post',
        data: params
    });
}

// 话题统计
export function discussStatistics(params = {}) {
    return request({
        url: 'statistics/discuss',
        method: 'post',
        data: params
    });
}