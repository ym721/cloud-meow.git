import request from "@/utils/request";

// 获取圈子列表-分页
export function getListByPage(params = {}) {
  return request({
    url: "topic/getListByPage",
    method: "post",
    data: params,
  });
}

// 获取圈子列表
export function getTopicList(params = {}) {
  return request({
    url: "topic/getList",
    method: "get",
    params,
  });
}

// 获取圈子分类
export function classList(params = {}) {
  return request({
    url: "category/getList",
    method: "get",
    params,
  });
}



// 删除圈子
export function topicDelete(params = {}) {
  return request({
    url: "topic/delete",
    method: "post",
    data: params
  });
}

// 获取圈子详情
export function topicInfo(params = {}) {
  return request({
    url: "topic/info",
    method: "get",
    params
  });
}

// 保存圈子信息
export function topicSave(params = {}) {
  return request({
    url: "topic/save",
    method: "post",
    data: params
  });
}

// 删除圈子
export function categoryDelete(params = {}) {
  return request({
    url: "category/delete",
    method: "post",
    data: params
  });
}

// 保存圈子类目
export function categorySave(params = {}) {
  return request({
    url: "category/save",
    method: "post",
    data: params
  });
}

// 圈子类目详情
export function categoryInfo(params = {}) {
  return request({
    url: "category/info",
    method: "get",
    params
  });
}