import request from '@/utils/request'

// 账号登录
export function accountLogin(params = {}) {
    return request({
        url: 'user/login',
        method: 'post',
        data: params
    });
}

// 获取登录用户菜单权限
export function queryMenuList() {
    return request({
        url: 'user/menuList',
        method: 'post'
    });
}


// 获取用户列表
export function userList(params = {}) {
    return request({
        url: 'user/getListByPage',
        method: 'POST',
        data:params
    });
}

// 获取角色列表
export function roleList(params = {}) {
    return request({
        url: 'user/roleList',
        method: 'get',
        params
    });
}

// 添加角色
export function roleAddApi(params = {}) {
    return request({
        url: 'user/roleAdd',
        method: 'get',
        params
    });
}

// 删除角色
export function roleDelApi(params = {}) {
    return request({
        url: 'user/roleDel',
        method: 'get',
        params
    });
}

// 保存用户角色权限
export function userRoleSave(params = {}) {
    return request({
        url: 'user/userRoleSave',
        method: 'post',
        data:params
    });
}

// 用户角色权限列表
export function userRuleList(params = {}) {
    return request({
        url: 'user/roleRuleList',
        method: 'get',
        params
    });
}

// 获取登录用户权限列表
export function ruleListAll(params = {}) {
    return request({
        url: 'user/ruleListAll',
        method: 'get',
        params
    });
}

// 删除权限规则
export function ruleDelete(params = {}) {
    return request({
        url: 'user/ruleDelete',
        method: 'post',
        data:params
    });
}

// 添加权限规则
export function ruleSave(params = {}) {
    return request({
        url: 'user/ruleSave',
        method: 'post',
        data: params
    });
}