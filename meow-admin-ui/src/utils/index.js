/**
 * 表格时间格式化
 */
export function formatDate(row, column, cellValue) {
    cellValue = cellValue * 1000

    if (cellValue == null || cellValue === "") return "";
    const date = new Date(cellValue)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
    const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
    const seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
    return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
}

/**
 * 
 * 重置表单数据
 * @param {*} data 
 */
export function reset(data) {
    const keys = Object.keys(data);
    let obj = {};
    keys.forEach((item) => {
        obj[item] = undefined;
    });
    Object.assign(data, obj);
}

//补全图片路径
export function imgPath(url) {
    if (!url) return
    if (url.includes('http://') || url.includes('https://')) {
        return url
    } else {
        return import.meta.env.VITE_IMAGE_URL + '/' + url + '?x-oss-process=image/resize,w_500,m_lfit'
    }
}

//补全图片路径-图片数组
export function imgArrayPath(urlArray) {
    let urls = [];
    urlArray.forEach(url=>{
        if (url.includes('http://') || url.includes('https://')) {
            urls.push(url)
        } else {
            urls.push(import.meta.env.VITE_IMAGE_URL + '/' + url + '?x-oss-process=image/resize,w_500,m_lfit')
        }
    })

    return urls;
}