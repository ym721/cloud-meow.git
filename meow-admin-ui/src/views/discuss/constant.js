// 用户列表字段
export const discussColumns = [
    {
        title: '标题',
        key: 'title',
    },
    {
        title: '帖子数',
        key: 'post_num',
        align:'center'
    },
    {
        title: '浏览数',
        key: 'see_num',
        align:'center'
    },
    {
        title: '创建时间',
        key: 'create_time',
        type:'date'
    },
    {
        title: '操作',
        type: 'operation',
        options: [
            {
                title: '删除',
                key: 'delete',
                type:'danger'
            }
        ]
    }
]

//用户列表查询条件

export const queryUserForm = [
    {
        label: '话题名称',
        value: '',
        type: 'input',
        key: 'title'
    }
]