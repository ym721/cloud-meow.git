// 用户列表字段
export const postTable = [
  {
    title: "用户",
    type: "user",
    width: 170,
  },
  {
    title: "内容",
    key: "content",
  },
  {
    title: "图片",
    type: "image_array",
    key: "media",
    align: "center",
    width: 100,
  },
  {
    title: "评论数",
    key: "comment_num",
    align: "center",
    width: 80,
  },
  {
    title: "浏览数",
    key: "see_num",
    align: "center",
    width: 80,
  },
  {
    title: "点赞数",
    key: "thumb_num",
    align: "center",
    width: 80,
  },
  {
    title: "创建时间",
    key: "create_time",
    type: "date",
  },
  {
    title: "操作",
    type: "operation",
    options: [
      {
        title: "删除",
        key: "delete",
        type: "danger",
      },
    ],
  },
];

//用户列表查询条件

export const queryForm = [
  {
    label: "所属圈子",
    value: "",
    type: "select",
    key: "topic_id",
    labelKey: "name",
    valueKey: "id",
  },
  {
    label: "帖子内容",
    value: "",
    type: "input",
    key: "content",
  }
];
