// 圈子列表字段
export const topicColumns = [
  {
    title: "类目",
    type: "dict",
    key: 'class_id',
    options: {},
  },
  {
    title: "圈子封面",
    key: "cover_image",
    type: "image",
  },
  {
    title: "圈子名称",
    key: "name",
  },
  {
    title: "圈主",
    type: "user",
  },
  {
    title: "用户数",
    key: "user_num",
    align: "center",
  },
  {
    title: "帖子数",
    key: "post_num",
    align: "center",
  },
  {
    title: "创建时间",
    key: "create_time",
    type: "date",
  },
  {
    title: "操作",
    type: "operation",
    options: [
      {
        title: "编辑",
        key: "edit",
        type: "primary"
      },
      {
        title: "删除",
        key: "delete",
        type: "danger"
      },
    ],
  },
];

//用户列表查询条件

export const queryUserForm = [
  {
    label: "类目",
    value: "",
    type: "select",
    key: "class_id",
    labelKey:'name',
    valueKey:'id',
    options: []
  },
  {
    label: "圈子名称",
    value: "",
    type: "input",
    key: "name",
  },
  {
    label: "状态",
    value: "",
    type: "select",
    key: "status",
    options: [
      {
        label: "关闭",
        value: 0,
      },
      {
        label: "正常",
        value: 1,
      },
      {
        label: "广场推荐",
        value: 2,
      },
    ],
  },
];
