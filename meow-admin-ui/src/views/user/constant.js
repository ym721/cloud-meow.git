// 用户列表字段
export const userColumns = [
  {
    title: "头像",
    key: "avatar",
    type: "image",
    width: "100",
    align: "center",
  },
  {
    title: "用户名",
    key: "nickname",
  },
  {
    title: "手机号",
    key: "mobile",
    align: "center",
  },
  {
    title: "帖子数",
    key: "post_num",
    align: "center",
  },
  {
    title: "粉丝数",
    key: "fans_num",
    align: "center",
  },
  {
    title: "关注数",
    key: "follow_num",
    align: "center",
  },
  {
    title: "状态",
    key: "is_disable",
    type: "dict",
    options: {
      0: "正常",
      1: "禁用",
    },
  },
  {
    title: "创建时间",
    key: "create_time",
    type: "date",
  }
];

//用户列表查询条件

export const queryForm = [
  {
    label: "用户昵称",
    value: "",
    type: "input",
    key: "nickname",
  },
  {
    label: "状态",
    value: "",
    type: "select",
    key: "is_disable",
    options: [
      {
        label: "正常",
        value: 0,
      },
      {
        label: "禁用",
        value: 1,
      },
    ],
  },
];
