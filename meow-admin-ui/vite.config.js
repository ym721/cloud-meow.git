import {
	defineConfig
} from 'vite'
import vue from '@vitejs/plugin-vue'
import {
	resolve
} from "path";

export default defineConfig({
	base: "./",
	resolve: {
		alias: [{
			find: '@',
			replacement: resolve(__dirname, 'src')
		},
		{
			find: /^~/,
			replacement: ''
		}
		]
	},
	plugins: [vue()],
	server: {
		port: '8081',
		open: false, //自动打开 
		// proxy: {
		// 	'^/adminapi': {
		// 		target: 'http://lite.a.com',
		// 		changeOrigin: true, //开启代理
		// 	}
		// }
	}
})
