<?php

namespace app\adminapi\controller;

use app\common\model\Admin as AdminModel;
class Admin extends BaseAdmin
{

    public function save()
    {
        $adminId = $this->adminId;
        $params = input("post.");

        $admin = AdminModel::where("id",$adminId)->findOrEmpty();

        if($admin->isEmpty()){
            return $this->fail("管理员信息错误");
        }

        $admin->username = $params["username"];
        
        if($params["password"]){
            $admin->password = md5($params["password"]);
        }

        $admin->save();

        return $this->success();
    }
}