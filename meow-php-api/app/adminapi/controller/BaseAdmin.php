<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\adminapi\controller;

use app\common\controller\BaseMeowAdmin;

/**
 * 管理后台基础类控制器
 */
class BaseAdmin extends BaseMeowAdmin
{

    protected int $adminId = 0;
    protected array $adminInfo = [];

    protected $dataModel;
    protected array $with = [];
    protected array $allowSearch = [];

    public function initialize($dataModel = null)
    {
        if (isset($this->request->adminInfo) && $this->request->adminInfo) {
            $this->adminInfo = $this->request->adminInfo;
            $this->adminId = $this->request->adminInfo['id'];
        }
    }

    /**
     * 获取数据列表-分页数据
     * @return mixed
     */
    public function getListByPage($order = "id desc")
    {
        $pageSize = input("pageSize", 10);
        $queryForm = input("queryForm", []);
        $searchWhere = array_intersect(array_keys($queryForm), $this->allowSearch);

        $list = $this->dataModel::withSearch($searchWhere, $queryForm)->with($this->with)->order($order)->paginate($pageSize);

        return $this->data($list);
    }

    /**
     * 获取数据列表
     * @return mixed
     */
    public function getList($where = [], $order = "id desc")
    {
        $list = $this->dataModel::where($where)->order($order)->select();

        return $this->data($list);
    }

    public function delete()
    {
        $ids = input("ids/a");
        $res = $this->dataModel::destroy($ids);

        if ($res) {
            return $this->success();
        }

        return $this->fail();
    }

    public function info()
    {
        $id = input("id");
        $info = $this->dataModel::where('id', $id)->find();

        return $this->data($info);
    }

    public function save()
    {
        $id = input("id");
        $params = input("post.");

        if($id){
            $dataModel =  $this->dataModel::find($id);
        }else{
            $dataModel = new $this->dataModel;
        }

        $dataModel->save($params);

        return $this->success();
    }

}