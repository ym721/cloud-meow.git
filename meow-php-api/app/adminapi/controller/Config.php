<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\adminapi\controller;

use app\common\model\Config as ConfigModel;

class Config extends BaseAdmin
{

    public function info()
    {
        $keyword = input("name");
        $info = ConfigModel::getInfo($keyword);

        return $this->data($info);
    }

    public function save()
    {
        $name = input("name");
        $value = input("value");

        $config = ConfigModel::where("name", $name)->findOrEmpty();

        if ($config->isEmpty()) {
            return $this->fail("配置信息错误");
        }

        $config->value = $value;
        $config->save();

        return $this->success();
    }
}