<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\adminapi\controller;

use app\common\model\Discuss as DiscussModel;

class Discuss extends BaseAdmin
{
    protected $dataModel = DiscussModel::class;
    protected array $allowSearch = ['title'];

    public function list()
    {
        $keyword = input("keyword");
        $where = [];

        if ($keyword) {
            $where[] = ['title', "like", "%$keyword%"];
        }

        $list = DiscussModel::getList($where);
        return $this->data($list);
    }
}