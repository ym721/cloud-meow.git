<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\adminapi\controller;

use think\exception\ValidateException;
use app\common\model\Link as LinkModel;
class Link extends BaseAdmin
{
    public function list()
    {
        $list = LinkModel::getList();
        return message($list);
    }

    public function save()
    {
        $data = input("post.");
        $id = input("id");

        try {
            validate(\app\common\validate\Link::class)->check($data);
        } catch (ValidateException $e) {
            return error($e->getError());
        }

        if (empty($id)) {
            $res = LinkModel::create($data);
        }else{
            $res = LinkModel::update($data);
        }

        if ($res){
            return message("保存成功");
        }

        return error("保存失败");
    }

    public function del(){
        $id = input("id");
        $res = LinkModel::destroy($id);
        if($res){
            return message("删除成功");
        }

        return error("删除失败");
    }
}