<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\adminapi\controller;

/**
 * 帖子控制器
 */

use app\common\model\Post as PostModel;


class Post extends BaseAdmin
{
    protected $dataModel = PostModel::class;
    protected array $with = ['userInfo']; //关联的数据模型
    protected array $allowSearch = ['topic_id','content','status'];
}