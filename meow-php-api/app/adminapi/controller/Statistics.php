<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\adminapi\controller;

/**
 * 统计类
 */

use app\common\service\Utils;
use think\facade\Db;

class Statistics extends BaseAdmin
{
    public function user()
    {
        $data['userCount'] = Db::name("user")->count();
        $data['postCount'] = Db::name("post")->count();
        $data['disCount'] = Db::name("discuss")->count();
        $data['commentCount'] = Db::name("comment")->count();

        $data["last_month"] = Db::name('user')->whereMonth('create_time', 'last month')->count();
        $data["month"] = Db::name('user')->whereMonth('create_time')->count();
        $data["week"] = Db::name('user')->whereWeek('create_time')->count();
        $data["day"] = Db::name('user')->whereDay('create_time')->count();

        //近30天日期列表
        $monthDateList = Utils::getDateInterval(30, '-');
        $monthValue = [];

        foreach ($monthDateList as $item) {
            $date = Db::name('user')->whereDay('create_time', $item)->count();
            array_push($monthValue, $date);
        }

        $month_data = [
            "lable" => $monthDateList,
            "value" => $monthValue
        ];

        $data["month_data"] = $month_data;
        return $this->data($data);
    }

    public function post()
    {
        $data['postCount'] = Db::name("post")->count();
        $data["last_month"] = Db::name('post')->whereMonth('create_time', 'last month')->count();
        $data["month"] = Db::name('post')->whereMonth('create_time')->count();
        $data["week"] = Db::name('post')->whereWeek('create_time')->count();
        $data["day"] = Db::name('post')->whereDay('create_time')->count();

        //近30天日期列表
        $monthDateList = Utils::getDateInterval(30, '-');
        $monthValue = [];

        foreach ($monthDateList as $item) {
            $date = Db::name('post')->whereDay('create_time', $item)->count();
            array_push($monthValue, $date);
        }

        $month_data = [
            "lable" => $monthDateList,
            "value" => $monthValue
        ];

        $data["month_data"] = $month_data;
        return $this->data($data);
    }

    public function discuss(){
        $data['disCount'] = Db::name("discuss")->count();
        $data["last_month"] = Db::name('discuss')->whereMonth('create_time', 'last month')->count();
        $data["month"] = Db::name('discuss')->whereMonth('create_time')->count();
        $data["week"] = Db::name('discuss')->whereWeek('create_time')->count();
        $data["day"] = Db::name('discuss')->whereDay('create_time')->count();

        //近30天日期列表
        $monthDateList = Utils::getDateInterval(30, '-');
        $monthValue = [];

        foreach ($monthDateList as $item) {
            $date = Db::name('discuss')->whereDay('create_time', $item)->count();
            array_push($monthValue, $date);
        }

        $month_data = [
            "lable" => $monthDateList,
            "value" => $monthValue
        ];

        $data["month_data"] = $month_data;
        return $this->data($data);
    }
}