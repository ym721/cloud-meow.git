<?php

namespace app\adminapi\controller;

use app\common\model\Topic as TopicModel;
use think\facade\Db;

class Topic extends BaseAdmin
{
    protected $dataModel = TopicModel::class;
    protected array $with = ["userInfo"];
    protected array $allowSearch = ['class_id','name','status'];

    public function list()
    {
        $keyword = input("keyword");

        $where = [];

        if ($keyword) {
            $where[] = ["name", "like", "%" . $keyword . "%"];
        }

        $list = TopicModel::getList($where);

        return $this->data($list);
    }
}