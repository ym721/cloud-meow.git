<?php

namespace app\api\controller;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

class AliCloud extends BaseApi
{

    //调用STS服务AssumeRole
    public function getAuth()
    {
        //构建一个阿里云客户端，用于发起请求。
        //设置调用者（RAM用户或RAM角色）的AccessKey ID和AccessKey Secret。
        AlibabaCloud::accessKeyClient('AccessKey', 'Secret')
            ->regionId('cn-hangzhou')
            ->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Sts')
                ->scheme('https') // https | http
                ->version('2015-04-01') //不用修改
                ->action('AssumeRole')//不用修改
                ->method('POST')//不用修改
                ->host('sts.aliyuncs.com')//不用修改
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'RoleArn' => "请填写RoleArn",
                        'RoleSessionName' => "请填写RoleSessionName",
                    ],
                ])
                ->request();

            return $this->data($result->toArray());
        } catch (ClientException $e) {
            return $this->fail($e->getErrorMessage());
        }
    }
}