<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\controller;


use app\common\controller\BaseMeowAdmin;
use app\common\model\User as UserModel;
use app\common\service\JsonService;
use app\common\service\wechat\WeChatMnpService;
use think\Exception\HttpResponseException;
use app\common\cache\UserTokenCache;

class BaseApi extends BaseMeowAdmin
{

    protected $request;
    protected $dataModel;

    protected array $allowSearch = [];
    protected array $with = [];

    public function initialize()
    {

    }

    public function getUserId()
    {
        $token = $this->request->header('token');

        if (empty($token)) {
            JsonService::throw('请求参数缺少token', 420);
        }

        $userInfo = (new UserTokenCache())->getUserInfo($token);

        if (empty($userInfo)) {
            JsonService::throw('登录超时，请重新登录', 420);
        }

        if ($userInfo["user_id"]) {
            return $userInfo["user_id"];
        }
        JsonService::throw('用户信息获取失败', 420);
    }

    public function getUserIdNoAuth()
    {
        $token = $this->request->header('token');

        $userInfo = (new UserTokenCache())->getUserInfo($token);

        if (!empty($userInfo) && $userInfo["user_id"]) {
            return $userInfo["user_id"];
        } else {
            return 0;
        }
    }

    /**
     * 检测用户账号
     * @return void
     */
    public function checkAccount()
    {
        $userId = $this->getUserId();
        $user = UserModel::where('id', $userId)->findOrEmpty();

        if ($user->isEmpty()) {
            JsonService::throw('登录用户不存在');
        }

        //检测账号是否可用
        if ($user->is_disable) {
            JsonService::throw('您的账号异常，请联系客服。');
        }
    }

    /**
     * 微信检测文本安全
     * @return void
     */
    public function wxCheckText($content)
    {
        $userId = $this->getUserId();
        $user = UserModel::where('id', $userId)->findOrEmpty();

        $checkText = (new WeChatMnpService())->msgSecCheck($user['openid'], $content);

        if (!$checkText) {
            JsonService::throw('内容包含敏感文字');
        }
    }

    public function delete()
    {
        $ids = input("ids/a");
        $res = $this->dataModel::destroy($ids);

        if ($res) {
            return $this->success();
        }

        return $this->fail();
    }

    public function info()
    {
        $id = input("id");
        $info = $this->dataModel::where('id', $id)->find();

        return $this->data($info);
    }

    public function save()
    {
        $id = input("id");
        $params = input("post.");

        if ($id) {
            $dataModel = $this->dataModel::find($id);
        } else {
            $params["user_id"] = $this->getUserId();
            $dataModel = new $this->dataModel;
        }

        $dataModel->save($params);

        return $this->success();
    }
}