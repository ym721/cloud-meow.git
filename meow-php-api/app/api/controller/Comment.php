<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\controller;

use app\common\model\User as UserModel;
use app\common\service\wechat\WeChatMnpService;
use think\facade\Db;
use app\common\model\Comment as CommentModel;
use app\common\service\Message;

class Comment extends BaseApi
{
    public array $notNeedLogin = ['list'];

    //评论列表
    public function list()
    {
        $postId = input("post_id");
        $type = input("type", '1');
        $uid = $this->getUserIdNoAuth();

        $where["pid"] = 0;
        $where["post_id"] = $postId;
        $where["type"] = $type;

        $list = CommentModel::withJoin(["userInfo"])->order("id desc")->where($where)->paginate(10)->each(function ($item) use ($uid) {

            $item["thumb_num"] = Db::name("comment_thumb")->where("c_id", $item["id"])->count();

            $item["is_thumb"] = false;
            $isCon = Db::name("comment_thumb")->where(["user_id" => $uid, "c_id" => $item["id"]])->find();
            if ($isCon) {
                $item["is_thumb"] = true;
            }

            $children = CommentModel::withJoin(["userInfo"])->where("pid", $item["id"])->select()->each(function ($item2) use ($uid) {
                $item2["to_user"] = Db::name("user")->where("id", $item2["to_uid"])->find();
                $item2["thumb_num"] = Db::name("comment_thumb")->where("c_id", $item2["id"])->count();

                $item2["is_thumb"] = false;
                $isCon = Db::name("comment_thumb")->where(["user_id" => $uid, "c_id" => $item2["id"]])->find();
                if ($isCon) {
                    $item2["is_thumb"] = true;
                }

            });

            if (count($children) > 0) {
                $item["children"] = $children;

            } else {
                $item["children"] = [];
            }

            return $item;

        });

        return $this->data($list);
    }

    //删除评论
    public function del()
    {
        $id = input("id");

        $commentInfo = CommentModel::where("id", $id)->find();
        $res = CommentModel::destroy($id);
        if ($res) {
            Db::name("post")->where("id", $commentInfo["post_id"])->dec('comment_num')->update();
            return $this->success("删除成功");
        }
        return $this->fail("删除失败");
    }

    //发表评论
    public function addComment()
    {

        $userId = $this->getUserId();
        $params = input("post.");
        $params["user_id"] = $userId;

        $user = UserModel::where('id', $userId)->findOrEmpty();

        if ($user->isEmpty()) {
            return $this->fail("登录用户不存在");
        }

        //检测账号是否可用
        if ($user->is_disable) {
            return $this->fail("您的账号异常，请联系客服。");
        }

        $checkText = (new WeChatMnpService())->msgSecCheck($user['openid'], $params["content"]);

        if (!$checkText) {
            return $this->fail('内容包含敏感文字');
        }

        $res = \app\common\model\Comment::create($params);

        if ($res) {
            $msgToUid = input("to_uid");

            if (empty($msgToUid)) {
                if ($params["type"] == 1) {
                    $msgToUid = Db::name("post")->where("id", $params["post_id"])->value("user_id");
                    Db::name("post")->where("id", $params["post_id"])->inc('comment_num')->update();
                    Message::send($userId, $msgToUid, $params["post_id"], 2, "评论：" . $params["content"]);
                }

                if ($params["type"] == 2) {
                    $msgToUid = Db::name("goods")->where("id", $params["post_id"])->value("user_id");
                    Db::name("post")->where("id", $params["post_id"])->inc('comment_num')->update();
                    Message::send($userId, $msgToUid, $params["post_id"], 7, $params["content"]);
                }
            }

            return $this->data($res);
        }

        return $this->fail("评论失败");
    }

    //评论点赞
    public function thumbAdd()
    {
        $id = input("id");

        $userId = $this->getUserId();

        $data["c_id"] = $id;
        $data["user_id"] = $userId;
        $isCon = Db::name("comment_thumb")->where($data)->find();
        if ($isCon) {
            return $this->fail("请勿重复点赞");
        }

        $data["create_time"] = time();

        $res = Db::name("comment_thumb")->insert($data);

        if ($res) {
            $comment = CommentModel::find($id);
            Message::send($userId, $comment["user_id"], $comment["post_id"], 1, "赞了您的评论：" . $comment["content"]);
            return $this->success("点赞成功");
        }

        return $this->fail("点赞失败");
    }

    //取消点赞
    public function cancelThumb()
    {
        $id = input("id");
        $userId = $this->getUserId();

        $where["c_id"] = $id;
        $where["user_id"] = $userId;

        $res = Db::name("comment_thumb")->where($where)->delete();
        if ($res) {
            return $this->success("已取消点赞");
        }

        return $this->fail("取消失败");
    }
}