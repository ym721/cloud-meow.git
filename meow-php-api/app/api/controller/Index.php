<?php

namespace app\api\controller;

class Index extends BaseApi
{
    public function index()
    {
        $info = '欢迎使用《云圈Lite》系统，官网：<a href="https://www.meoyun.com">https://www.meoyun.com</a>';
        echo '<div style="font-size:30px;text-align: center">'.$info.'</div>';
    }
}