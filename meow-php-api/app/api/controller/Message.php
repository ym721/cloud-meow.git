<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\controller;

use think\facade\Db;
use app\common\model\Message as MessageModel;

class Message extends BaseApi
{
    public array $notNeedLogin = ['num'];

    public function list()
    {
        $msgType = input("type");
        $userId = $this->getUserId();

        if ($msgType == 1) {
            $where[] = ["to_uid", "=", $userId];
            $where[] = ["type", "in", [1, 3]];
        }

        if ($msgType == 2) {
            $where[] = ["to_uid", "=", $userId];
            $where[] = ["type", "in", [4]];
        }

        if ($msgType == 3) {
            $where[] = ["to_uid", "=", $userId];
            $where[] = ["type", "in", [2, 7]];
        }

        $list = MessageModel::order("create_time desc")->where($where)->with(["userInfo", "postInfo", "goodsInfo"])->paginate(10);

        return $this->data($list);
    }

    public function status()
    {
        $msgType = input("type");

        $uid = $this->getUserId();

        $where[] = ["to_uid", "=", $uid];

        if ($msgType == 1) {
            $where[] = ["type", "in", [1, 3]];
        }

        if ($msgType == 2) {
            $where[] = ["type", "=", 4];
        }

        if ($msgType == 3) {
            $where[] = ["type", "in", [2, 7]];
        }
        if ($msgType == 5) {
            $where[] = ["type", "=", 5];
        }
        if ($msgType == 6) {
            $where[] = ["type", "=", 6];
        }

        $res = Db::name("message")->where($where)->update(["status" => 1]);

        if ($res) {
            return $this->success("消息状态修改成功");
        }
        return $this->fail("消息状态修改失败");
    }

    public function num()
    {

        $userId = $this->getUserIdNoAuth();
        $where["status"] = 0;
        $where["to_uid"] = $userId;

        //消息数量
        $thumbCollect = Db::name("message")->where($where)->where("type", "in", [1, 3])->count();
        $follow = Db::name("message")->where($where)->where("type", 4)->count();
        $comment = Db::name("message")->where($where)->where("type", 'in', [2, 7])->count();
        $allCount = Db::name("message")->where($where)->count();

        //公众号图文消息
        $article_msg_list = MessageModel::where("to_uid", $userId)->where('type', 6)->with(['userInfo'])->select();

        //帖子违规消息
        $chatUids = Db::name("message")->where("to_uid", $userId)->where("type", 5)->order("create_time desc")->column("from_uid");

        $chatUids = array_unique($chatUids);

        $postNotice = [];
        $whereNotice["to_uid"] = $userId;
        foreach ($chatUids as $chatUserId) {
            $whereNotice["from_uid"] = $chatUserId;
            $whereNotice["type"] = 5;

            $msg["count"] = Db::name("message")->where($whereNotice)->where("status", 0)->count();
            $msg["msg"] = Db::name("message")->where($whereNotice)->order("create_time desc")->limit(1)->find();
            $msg["user_info"] = Db::name("user")->where("id", $chatUserId)->find();

            array_push($postNotice, $msg);
        }

        $data = [
            "thumb_collect" => $thumbCollect,
            "follow" => $follow,
            "comment" => $comment,
            "all_count" => $allCount,
            "article_msg_list" => $article_msg_list,
            "post_notice" => $postNotice
        ];

        return $this->data($data);
    }

    //获取帖子相关通知消息
    public function chatList()
    {
        $toUid = input("user_id");
        $fromUid = $this->getUserId();

        $where1["to_uid"] = $toUid;
        $where1["from_uid"] = $fromUid;

        $where2["to_uid"] = $fromUid;
        $where2["from_uid"] = $toUid;

        $toList = Db::name("message")->where("type", 5)->where($where1)->select()->toArray();
        $fromList = Db::name("message")->where("type", 5)->where($where2)->select()->toArray();

        $list = array_merge($toList, $fromList); //合并数组
        $temps = array_column($list, 'id');

        array_multisort($list, SORT_ASC, $temps);

        return $this->data($list);
    }

    //图文消息更改阅读状态
    public function articleMsgState()
    {
        $id = input("post_id");
        Db::name("message")->where("id", $id)->update(["status" => 1]);
    }

    //更新通知消息状态
    public function updateChatStatus()
    {
        $where["from_uid"] = input("user_id");
        $where["to_uid"] = $this->getUserId();
        $where["type"] = 5;

        Db::name("message")->where($where)->update(["status" => 1]);
    }

    //删除通知消息
    public function delChat()
    {
        $uid = input("user_id");

        $where["from_uid"] = $uid;
        $where["to_uid"] = $this->getUserId();
        $where["type"] = 5;

        $res = MessageModel::where($where)->delete();

        if ($res) {
            return $this->success();
        }

        return $this->fail();
    }

    //删除图文消息
    public function delMsg()
    {
        $id = input("id", "", "intval");
        $res = MessageModel::destroy($id);

        if ($res) {
            return $this->success();
        }
        return $this->fail();
    }
}