<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\controller;

use app\common\service\AliyunOssService;
use app\common\service\FileService;
use OSS\OssClient;
use OSS\Core\OssException;

class Upload extends BaseApi
{

    /**
     * @notes 上传图片
     */
    public function image()
    {
        $fileUrl = (new AliyunOssService())->upload('upload/' . date('Y') . '/' . date('m') . '/' . date('d'));

        return $this->data([
            "url" => $fileUrl,
            "uri" => FileService::getFileUrl($fileUrl)
        ]);
    }
}