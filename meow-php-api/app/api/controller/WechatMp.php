<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\controller;

use app\common\service\wechat\WeChatMnpService;
use think\facade\Log;
use think\facade\Db;

/**
 * 微信小程序类
 */
class WechatMp extends BaseApi
{

    public function wxService()
    {
        $echostr = input("get.echostr");
        $server = (new WeChatMnpService())->getServer();

        // 确认此次GET请求来自微信服务器，原样返回echostr参数内容，接入生效，成为开发者成功
        if (isset($echostr)) {
            return Response($echostr)->header([
                'Content-Type' => 'text/plain;charset=utf-8'
            ]);
        }

        $encryptMessage = $server->getDecryptedMessage()->toArray();

        $event = $encryptMessage["Event"];

        //图片安全检测
        if ($event == 'wxa_media_check') {
            $detailMsg = $encryptMessage["detail"];

            if ($detailMsg["suggest"] != 'pass') {
                $mediaCheck = Db::name("media_check")->where("trace_id", $encryptMessage["trace_id"])->find();

                if ($mediaCheck["type"] == 1) {
                    Db::name("post")->where("id", $mediaCheck["post_id"])->update(["media" => []]);
                }

                if ($mediaCheck["type"] == 2) {
                    Db::name("goods")->where("id", $mediaCheck["post_id"])->update(["img_list" => []]);
                }
            }

            Db::name("media_check")->where("trace_id", $encryptMessage["trace_id"])->update(['is_risky' => $detailMsg["suggest"]]);
        }

        $response = $server->serve();

        return $response;
    }
}