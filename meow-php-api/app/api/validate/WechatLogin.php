<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\api\validate;

use app\common\validate\BaseValidate;

class WechatLogin extends BaseValidate
{
    protected $rule = [
        'code' => 'require',
        'nickname' => 'require',
        'openid' => 'require',
        'terminal' => 'require',
        'avatar' => 'require',
    ];

    protected $message = [
        'code.require' => 'code缺少',
        'nickname.require' => '昵称缺少',
        'openid.require' => 'opendid缺少',
        'terminal.require' => '终端参数缺少',
        'avatar.require' => '头像缺少',
    ];

    /**
     * @notes 小程序-授权登录场景
     */
    public function sceneMnpLogin()
    {
        return $this->only(['code']);
    }
}