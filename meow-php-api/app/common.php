<?php
// 应用公共文件

/**
 * @notes 去除内容图片域名
 * @param $content
 * @return array|string|string[]
 * @author 段誉
 * @date 2022/9/26 10:43
 */
function clear_file_domain($content)
{
    $fileUrl = "https://www-meoyun-com.oss-cn-guangzhou.aliyuncs.com/";
    return str_replace($fileUrl, '/', $content);
}


/**
 * @notes 设置内容图片域名
 * @param $content
 * @return array|string|string[]|null
 * @author 段誉
 * @date 2022/9/26 10:43
 */
function get_file_domain($content)
{
    $preg = '/(<img .*?src=")[^https|^http](.*?)(".*?>)/is';
    $fileUrl = "https://www-meoyun-com.oss-cn-guangzhou.aliyuncs.com/";
    return preg_replace($preg, "\${1}$fileUrl\${2}\${3}", $content);
}