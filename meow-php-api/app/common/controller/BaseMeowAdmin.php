<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------

namespace app\common\controller;

use app\BaseController;
use think\response\Json;

class BaseMeowAdmin extends BaseController
{

    protected function success(string $msg = 'success', int $code = 1): Json
    {
        $result = compact('code', 'msg');

        return json($result);
    }

    protected function fail(string $msg = '系统发生错误，请稍后再试', int $show = 1, int $code = 0): Json
    {
        $result = compact('code', 'msg', 'show');

        return json($result);
    }

    protected function data($result, string $msg = 'success', int $code = 1): Json
    {
        $result = compact('code', 'msg', 'result');

        return json($result);
    }
}