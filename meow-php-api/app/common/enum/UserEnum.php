<?php

namespace app\common\enum;

/**
 * 用户相关枚举
 */
class UserEnum
{
    /**
     * 用户终端
     */
    const WECHAT_MMP = 1; //微信小程序
    const WECHAT_OA = 2; //微信公众号
    const H5 = 3;//手机H5登录
    const PC = 4;//电脑PC
    const IOS = 5;//苹果app
    const ANDROID = 6;//安卓app


    /**
     * 性别
     * SEX_OTHER = 未知
     * SEX_MEN =  男
     * SEX_WOMAN = 女
     */
    const SEX_OTHER = 0;
    const SEX_MEN = 1;
    const SEX_WOMAN = 2;


    /**
     * @notes 性别描述
     * @param bool $from
     * @return string|string[]
     * @author 段誉
     * @date 2022/9/7 15:05
     */
    public static function getSexDesc($from = true)
    {
        $desc = [
            self::SEX_OTHER => '未知',
            self::SEX_MEN => '男',
            self::SEX_WOMAN => '女',
        ];
        if (true === $from) {
            return $desc;
        }
        return $desc[$from] ?? '';
    }
}