<?php


namespace app\common\model;

class Comment extends BaseModel
{
    public function userInfo()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function childrenList()
    {
        return $this->hasMany(CommentReply::class, 'comment_id', 'id');
    }
}
