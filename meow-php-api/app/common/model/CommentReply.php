<?php

namespace app\common\model;

use think\Model;
use app\common\model\user\User as ModelUser;

/**
 * 回复模型
 */
class CommentReply extends BaseModel
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'tn_comment_reply';

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';


    public function fromUser()
    {
        return $this->hasone(ModelUser::class, 'uid', 'from_uid')->field(['uid', 'username', 'avatar']);
    }

    public function toUser()
    {
        return $this->hasone(ModelUser::class, 'uid', 'to_uid')->field(['uid', 'username', 'avatar']);
    }
}
