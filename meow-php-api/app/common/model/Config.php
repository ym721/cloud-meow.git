<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\model;

class Config extends BaseModel
{
    protected $json = ['value'];
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;


    public static function getInfo(string $name)
    {
        return self::where('name', $name)->value('value');
    }
}