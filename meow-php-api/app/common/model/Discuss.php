<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\model;

class Discuss extends BaseModel
{
    public function userInfo()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    /**
     * 标题-搜索器
     * @param $query
     * @param $value
     * @return void
     */
    public function searchTitleAttr($query, $value)
    {
        $query->where('title', 'like', $value . '%');
    }
}