<?php

namespace app\common\model;

class Goods extends BaseModel
{
    protected $json = ['img_list'];
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    public function userInfo()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function topicInfo()
    {
        return $this->hasOne(Topic::class, "id", "topic_id");
    }
}