<?php


namespace app\common\model;


class Message extends BaseModel
{
    public function userInfo()
    {
        return $this->hasOne(User::class, "id", "from_uid");
    }

    public function postInfo()
    {
        return $this->hasOne("Post", "id", "post_id");
    }

    public function goodsInfo()
    {
        return $this->hasOne("Goods", "id", "post_id");
    }
}