<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\common\model;

use app\common\service\FileService;

/**
 * Topic模型
 * Class TopicValidate
 * @package app\common\model
 */
class Topic extends BaseModel
{
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    /**
     * 圈子背景-获取器
     * @param $value
     * @return mixed|string
     */
    public function getBgImageAttr($value)
    {
        return FileService::getFileUrl($value);
    }

    /**
     * 圈子背景-修改器
     * @param $value
     * @return array|string|string[]
     */
    public function setBgImageAttr($value)
    {
        return FileService::setFileUrl($value);
    }

    /**
     * 圈子封面-获取器
     * @param $value
     * @return mixed|string
     */
    public function getCoverImageAttr($value)
    {
        return FileService::getFileUrl($value);
    }

    /**
     * 圈子封面-修改器
     * @param $value
     * @return array|string|string[]
     */
    public function setCoverImageAttr($value)
    {
        return FileService::setFileUrl($value);
    }

    public function userInfo()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }


    /**
     * 名称-搜索器
     * @param $query
     * @param $value
     * @return void
     */
    public function searchNameAttr($query, $value)
    {
        $query->where('name', 'like', $value . '%');
    }

    /**
     * 状态-搜索器
     * @param $query
     * @param $value
     * @return void
     */
    public function searchStatusAttr($query, $value)
    {
        $query->where('status', '=', $value);
    }

    /**
     * 类目-搜索器
     * @param $query
     * @param $value
     * @return void
     */
    public function searchClassIdAttr($query, $value)
    {
        $query->where('class_id', '=', $value);
    }
}