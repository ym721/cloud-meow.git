<?php

namespace app\common\model;

class TopicGroup extends BaseModel
{
    protected $json = ['wechat', 'qq'];
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;
}