<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\model;

use app\common\service\FileService;

use app\common\enum\UserEnum;

class User extends BaseModel
{

    protected $json = ['tag_str'];
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;

    /**
     * 头像-获取器
     * @param $value
     * @return mixed|string
     */
    public function getAvatarAttr($value)
    {
        return FileService::getFileUrl($value);
    }

    /**
     * 头像-修改器
     * @param $value
     * @return array|string|string[]
     */
    public function setAvatarAttr($value)
    {
        return FileService::setFileUrl($value);
    }

    /**
     * 性别-获取器
     * @param $value
     * @param $data
     * @return string|string[]
     */
    public function getSexAttr($value, $data)
    {
        return UserEnum::getSexDesc($value);
    }

    /**
     * 昵称-搜索器
     * @param $query
     * @param $value
     * @return void
     */
    public function searchNickNameAttr($query, $value)
    {
        $query->where('nickname', 'like', $value . '%');
    }

    /**
     * 禁用状态-搜索器
     * @param $query
     * @param $value
     * @return void
     */
    public function searchIsDisableAttr($query, $value)
    {
        $query->where('is_disable', '=', $value);
    }
}