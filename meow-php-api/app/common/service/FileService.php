<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service;

use app\common\model\Config;

/**
 * 文件服务类
 */
class FileService
{

    /**
     * @notes 补全路径
     */
    public static function getFileUrl($imgSrc)
    {
        if (str_contains($imgSrc, 'http://')) return $imgSrc;
        if (str_contains($imgSrc, 'https://')) return $imgSrc;

        $storageConfig = Config::getInfo('storage_config');
        return $storageConfig["domain"] . '/' . $imgSrc;
    }

    /**
     * @notes 转相对路径
     */
    public static function setFileUrl($uri)
    {
        $storageConfig = Config::getInfo('storage_config');
        return str_replace($storageConfig["domain"] . '/', '', $uri);
    }
}