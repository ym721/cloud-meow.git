<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service;

use think\Response;
use think\response\Json;
use think\exception\HttpResponseException;

class JsonService
{
    public static function throw(string $msg = 'fail', int $code = 0, int $show = 1): Json
    {
        $data = compact('code', 'msg', 'show');
        $response = Response::create($data, 'json');
        throw new HttpResponseException($response);
    }

    public static function success(string $msg = 'success', int $code = 1): Json
    {
        $result = compact('code', 'msg');

        return json($result);
    }

    public static function fail(string $msg = '系统发生错误，请稍后再试', int $show = 1, int $code = 0): Json
    {
        $result = compact('code', 'msg', 'show');

        return json($result);
    }

    public static function data($result, string $msg = 'success', int $code = 1): Json
    {
        $result = compact('code', 'msg', 'result');

        return json($result);
    }
}