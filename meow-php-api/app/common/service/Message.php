<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2022 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service;

/**
 * 消息服务类
 */

use app\common\model\User as UserModel;
use app\common\service\wechat\WeChatMnpService;
use think\facade\Db;

class Message
{

    public static function send($from_uid, $to_uid, $post_id, $type, $content = "", $title = "", $tag = '')
    {

        /**
         * type 1 点赞，2 评论  3 收藏 4 关注  5 违规帖子通知 6 公众号图文通知  7 商品评论
         */

        $data["from_uid"] = $from_uid;
        $data["to_uid"] = $to_uid;
        $data["post_id"] = $post_id;
        $data["title"] = $title;
        $data["content"] = $content;
        $data["tag"] = $tag;
        $data["type"] = $type;
        $data["create_time"] = time();

        $where["from_uid"] = $from_uid;
        $where["to_uid"] = $to_uid;
        $where["type"] = $type;
        $where["post_id"] = $post_id;
        $where["content"] = $content;

        if ($from_uid != $to_uid) {
            $res = Db::name("message")->where($where)->find();
            if (!$res) {

                //点赞获得经验与积分
                if ($type == 1 || $type == 3) {
                    Db::name('user')->where('id', $from_uid)->inc('exp', 1)->update();
                    Db::name('user')->where('id', $from_uid)->inc('integral', 1)->update();
                }

                //评论获得经验与积分
                if ($type == 2) {
                    Db::name('user')->where('id', $from_uid)->inc('exp', 5)->update();
                    Db::name('user')->where('id', $from_uid)->inc('integral', 5)->update();
                }

                if ($type == 7) {
                    //发送模板消息
                    $tmplId = config('project.subscribe.replyTmplId');
                    $page = 'pages/post/detail?id=' . $post_id;
                    $goodsTile = Db::name("goods")->where("id", $post_id)->value("title");
                    $toUser = UserModel::where('id', $to_uid)->findOrEmpty();
                    $fromUser = UserModel::where('id', $from_uid)->findOrEmpty();
                    (new  WeChatMnpService())->sendSubscribeMessage($tmplId, $toUser['openid'], $page, [
                        'thing6' => [
                            "value" => $goodsTile
                        ],
                        'thing2' => [
                            "value" => $content
                        ],
                        'thing5' => [
                            "value" => $fromUser["nickname"]
                        ],
                        'time3' => [
                            "value" => date("Y-m-d")
                        ]
                    ]);
                }

                Db::name("message")->insert($data);
            }
        }
    }
}