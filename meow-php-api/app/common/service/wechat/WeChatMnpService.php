<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\service\wechat;

use app\common\model\Config;
use EasyWeChat\Kernel\Exceptions\Exception;
use EasyWeChat\MiniApp\Application;

/**
 * 微信小程序服务类
 */
class WeChatMnpService
{
    protected $app;

    protected $config;

    public function __construct()
    {
        $this->config = $this->getConfig();
        $this->app = new Application($this->config);
    }

    public function getServer()
    {
        return $this->app->getServer();
    }

    /**
     * @notes 配置
     */
    protected function getConfig()
    {
        $mnpConfig = Config::getInfo("wechat_mnp");
        if (empty($mnpConfig['app_id']) || empty($mnpConfig['secret'])) {
            throw new Exception("请先设置小程序配置");
        }
        return $mnpConfig;
    }


    /**
     * @notes 小程序-根据code获取微信信息
     * @param string $code
     */
    public function getMnpResByCode(string $code)
    {
        $utils = $this->app->getUtils();
        $response = $utils->codeToSession($code);

        if (!isset($response['openid']) || empty($response['openid'])) {
            throw new Exception('获取openID失败');
        }

        return $response;
    }


    /**
     * @notes 获取手机号
     * @param string $code
     */
    public function getUserPhoneNumber(string $code)
    {
        return $this->app->getClient()->postJson('wxa/business/getuserphonenumber', [
            'code' => $code,
        ]);
    }


    /**
     * @notes 获取小程序二维码
     * @param string $code
     */
    public function getQrCode(string $page, string $scene)
    {
        return $this->app->getClient()->postJson('wxa/getwxacodeunlimit', [
            'page' => $page,
            'scene' => $scene
        ]);
    }

    /**
     * @notes 检查一段文本是否含有违法违规内容
     * @param $openid
     * @param $content
     */
    public function msgSecCheck($openid, $content)
    {
        $checkResJson = $this->app->getClient()->postJson('wxa/msg_sec_check', [
            'openid' => $openid,
            'content' => $content,
            'scene' => 3,
            'version' => 2,
        ]);
        $checkRes = json_decode($checkResJson);

        if ($checkRes->result->label == 100) {
            return true;
        }

        return false;
    }

    /**
     * @notes 异步校验图片/音频是否含有违法违规内容
     * @param $openid
     * @param $content
     */
    public function mediaCheckAsync($openid, $mediaUrl, $mediaType = 2)
    {
        return $this->app->getClient()->postJson('wxa/media_check_async', [
            'openid' => $openid,
            'media_url' => $mediaUrl,
            'media_type' => $mediaType, // 1:音频;2:图片
            'version' => 2,
            'scene' => 3,
        ]);
    }

    /**
     * 发送订阅消息
     */
    public function sendSubscribeMessage(string $templateId, $touser, $page, $data)
    {
        return $this->app->getClient()->postJson('cgi-bin/message/subscribe/send', [
            'template_id' => $templateId,
            'page' => $page,
            'touser' => $touser,
            'data' => $data,
            'miniprogram_state' => 'formal',
            'lang' => 'zh_CN'
        ]);
    }

    /**
     * 获取ShortLink
     */
    public function getShortLink(string $pageUrl)
    {
        return $this->app->getClient()->postJson('wxa/genwxashortlink', [
            'page_url' => $pageUrl
        ]);
    }

    /**
     * 获取scheme码
     */
    public function generateScheme(string $pagePath = '', $query = '')
    {
        return $this->app->getClient()->postJson('wxa/generatescheme', [
            'jump_wxa' => [
                'path' => $pagePath,
                'query' => $query,
//                'env_version' => 'trial'
            ]
        ]);
    }

    /**
     * 查询scheme码
     */
    public function queryscheme($scheme)
    {
        return $this->app->getClient()->postJson('wxa/queryscheme', [
            'scheme' => $scheme
        ]);
    }
}