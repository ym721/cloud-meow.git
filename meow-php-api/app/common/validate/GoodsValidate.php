<?php

namespace app\common\validate;

class GoodsValidate extends BaseValidate
{
    protected $rule = [
        'title' => 'require',
        'description' => 'require',
        'price' => 'require',
        'img_list' => 'require'
    ];

    protected $message = [
        'content.title' => '请填写商品标题',
        'content.description' => '请填写商品简介',
        'content.price' => '请填写商品价格',
        'content.img_list' => '请上传商品图片'
    ];
}