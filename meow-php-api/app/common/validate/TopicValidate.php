<?php


namespace app\common\validate;


class TopicValidate extends BaseValidate
{
    protected $rule = [
        'name' => 'require|unique:topic|chsAlpha',
        'description' => 'require'
    ];

    protected $message = [
        'name.require' => '圈子名称必填',
        'name.unique' => '该圈子名称已经存在',
        'name.chsAlpha' => '圈子名称只能为汉字和字母',
        'description.require' => '圈子描述必填'
    ];
}