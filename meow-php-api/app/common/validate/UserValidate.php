<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2019~2023 https://www.meoyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 喵云科技 【https://www.meoyun.com】
// +----------------------------------------------------------------------


namespace app\common\validate;

class UserValidate extends BaseValidate
{
    protected $rule = [
        'nickname' => 'require|unique:user',
        'avatar' => 'require',
    ];

    protected $message = [
        'nickname.require' => '用户昵称必填',
        'nickname.unique' => '该用户昵称已经存在',
        'avatar' => '请上传头像'
    ];

    /**
     * @notes 更新用户信息场景
     */
    public function sceneUpdateUser()
    {
        return $this->only(['nickname', 'avatar']);
    }
}