/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : test-db

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 03/08/2023 11:08:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for meow_admin
-- ----------------------------
DROP TABLE IF EXISTS `meow_admin`;
CREATE TABLE `meow_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` int(1) NOT NULL DEFAULT 0,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `root` int(255) NULL DEFAULT 0,
  `login_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of meow_admin
-- ----------------------------
INSERT INTO `meow_admin` VALUES (1, 0, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, '218.63.133.37', 1691025215);

-- ----------------------------
-- Table structure for meow_admin_group
-- ----------------------------
DROP TABLE IF EXISTS `meow_admin_group`;
CREATE TABLE `meow_admin_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of meow_admin_group
-- ----------------------------
INSERT INTO `meow_admin_group` VALUES (1, '超级管理员', NULL);

-- ----------------------------
-- Table structure for meow_admin_group_rule
-- ----------------------------
DROP TABLE IF EXISTS `meow_admin_group_rule`;
CREATE TABLE `meow_admin_group_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_admin_rule
-- ----------------------------
DROP TABLE IF EXISTS `meow_admin_rule`;
CREATE TABLE `meow_admin_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(2) NULL DEFAULT 0,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `route_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `components_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `api_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(2) NOT NULL DEFAULT 0 COMMENT '1 菜单，2 接口',
  `status` int(1) NULL DEFAULT 1 COMMENT '0 关闭，1启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50441 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of meow_admin_rule
-- ----------------------------
INSERT INTO `meow_admin_rule` VALUES (1, 0, '工作台', '', '', '', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (2, 0, '用户', '', 'Avatar', NULL, NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (3, 0, '内容', '', '', NULL, NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (5, 0, '设置', '', '', NULL, NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (101, 1, '用户统计', '/index', 'UserFilled', 'data/user', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (102, 1, '帖子统计', '/data/post', 'Promotion', 'data/post', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (103, 1, '话题统计', '/data/discuss', 'Comment', 'data/discuss', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (201, 2, '用户管理', '/user/list', 'Avatar', 'user/list', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (301, 3, '帖子管理', '/post/list', 'Promotion', 'post/list', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (302, 3, '话题管理', '/discuss/list', 'List', 'discuss/list', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (303, 3, '圈子管理', '/topic/list', '', 'topic/list', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (304, 3, '类目管理', '/topic/class', 'Menu', 'topic/class', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (502, 5, '微信小程序', '/seting/miniapp', 'Menu', 'seting/miniapp', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (503, 5, '上传配置', '/seting/upload', 'UploadFilled', 'seting/upload', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (504, 5, '权限设置', '#', 'Avatar', NULL, NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (1011, 101, '查看', '', '', NULL, 'statistics/user', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (1021, 102, '查看', '', '', NULL, 'statistics/post', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (1031, 103, '查看', '', '', NULL, 'statistics/discuss', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (2011, 201, '查看', '', '', NULL, 'user/getListByPage', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (3011, 301, '删除', '', '', NULL, 'post/delete', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (3012, 301, '查看', '', '', NULL, 'post/getListByPage', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (3021, 302, '删除', '', '', NULL, 'discuss/delete', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (3022, 302, '查看', '', '', NULL, 'discuss/getListByPage', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (3031, 303, '查看', '', '', NULL, 'topic/getListByPage', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (3041, 304, '删除', '', '', NULL, 'category/delete', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (3042, 304, '查看', '', '', NULL, 'category/getList', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (5021, 502, '查看', '', '', NULL, 'system/info', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (5022, 502, '保存', '', '', NULL, 'system/save', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (5041, 504, '角色管理', '/seting/role', 'Stamp', 'seting/role', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (5042, 504, '管理员', '/seting/administrator', 'Avatar', 'seting/administrator', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (5043, 504, '权限规则', '/seting/rule', '', 'seting/rule', NULL, 1, 1);
INSERT INTO `meow_admin_rule` VALUES (50411, 5041, '查询', '', '', NULL, 'user/roleList', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50412, 5041, '添加', '', '', NULL, 'user/roleAdd', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50413, 5041, '删除', '', '', NULL, 'user/roleDel', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50414, 5041, '角色权限查看', '', '', NULL, 'user/roleRuleList', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50415, 5041, '权限规则保存', '', '', NULL, 'user/ruleSave', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50421, 5042, '查询', '', '', NULL, 'user/adminList', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50422, 5042, '删除', '', '', NULL, 'user/delAdmin', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50423, 5042, '保存', '', '', NULL, 'user/save', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50433, 5043, '查看权限规则列表', '', '', NULL, 'user/ruleListAll', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50436, 5043, '删除权限规则', '', '', NULL, 'user/ruleDelete', 2, 1);
INSERT INTO `meow_admin_rule` VALUES (50440, 5043, '添加/编辑权限规则', '', '', NULL, 'user/userRoleSave', 2, 1);

-- ----------------------------
-- Table structure for meow_admin_session
-- ----------------------------
DROP TABLE IF EXISTS `meow_admin_session`;
CREATE TABLE `meow_admin_session`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `token` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '令牌',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `expire_time` int(10) NOT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_id_client`(`admin_id`) USING BTREE COMMENT '一个用户在一个终端只有一个token',
  UNIQUE INDEX `token`(`token`) USING BTREE COMMENT 'token是唯一的'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户会话表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_category
-- ----------------------------
DROP TABLE IF EXISTS `meow_category`;
CREATE TABLE `meow_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of meow_category
-- ----------------------------
INSERT INTO `meow_category` VALUES (1, '校园');
INSERT INTO `meow_category` VALUES (2, '兴趣');
INSERT INTO `meow_category` VALUES (3, '同城');

-- ----------------------------
-- Table structure for meow_comment
-- ----------------------------
DROP TABLE IF EXISTS `meow_comment`;
CREATE TABLE `meow_comment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT 0,
  `type` int(1) NOT NULL DEFAULT 1 COMMENT '评论类型:1帖子',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '评论作者ID',
  `to_uid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '被回复用户ID',
  `post_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '评论帖子ID',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '评论内容',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '评论状态',
  `create_time` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meow_comment_thumb
-- ----------------------------
DROP TABLE IF EXISTS `meow_comment_thumb`;
CREATE TABLE `meow_comment_thumb`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL COMMENT '评论id',
  `user_id` int(11) NOT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_config
-- ----------------------------
DROP TABLE IF EXISTS `meow_config`;
CREATE TABLE `meow_config`  (
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '值',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of meow_config
-- ----------------------------
INSERT INTO `meow_config` VALUES ('storage_config', '{\"bucket\":\"q-meoyun-com\",\"access_key\":\"LTAI5tK3vJrP8aDCtCeQGo8n\",\"secret_key\":\"7DiFSH9z1Z14Wr81H4gui6iOPC85wF\",\"domain\":\"https:\\/\\/q-meoyun-com.oss-cn-shenzhen.aliyuncs.com\"}', 1678346009, 1682325912);
INSERT INTO `meow_config` VALUES ('wechat_mnp', '{\"app_id\":\"wx88d4c53514cf8542\",\"secret\":\"7943053d0fd3f5ea312b95822a8f6640\",\"token\":\"T8DFG7D57DFG587YGHJYUAD\",\"aes_key\":\"1CLKa7iSON5WCfIBs0iPKICd3LOzwrRUxXTQGgNHZpE\"}', 1678333625, 1682949816);

-- ----------------------------
-- Table structure for meow_discuss
-- ----------------------------
DROP TABLE IF EXISTS `meow_discuss`;
CREATE TABLE `meow_discuss`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(32) NOT NULL,
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `post_num` int(11) NULL DEFAULT 0,
  `see_num` int(255) NOT NULL DEFAULT 0,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meow_follow
-- ----------------------------
DROP TABLE IF EXISTS `meow_follow`;
CREATE TABLE `meow_follow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `follow_uid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(10) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid`(`user_id`, `follow_uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meow_goods
-- ----------------------------
DROP TABLE IF EXISTS `meow_goods`;
CREATE TABLE `meow_goods`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `title` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `img_list` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wechat` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qq` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_media_check
-- ----------------------------
DROP TABLE IF EXISTS `meow_media_check`;
CREATE TABLE `meow_media_check`  (
  `trace_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `post_id` int(11) NOT NULL,
  `is_risky` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '等待检测结果',
  `media_src` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`trace_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_message
-- ----------------------------
DROP TABLE IF EXISTS `meow_message`;
CREATE TABLE `meow_message`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `to_uid` int(11) NOT NULL,
  `post_id` int(11) NULL DEFAULT NULL,
  `from_uid` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '推送标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0未读，1已读',
  `tag` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `type` int(1) NOT NULL COMMENT '1 点赞，2 评论  3 收藏 4 关注  5 违规帖子通知 6 公众号图文通知',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meow_post
-- ----------------------------
DROP TABLE IF EXISTS `meow_post`;
CREATE TABLE `meow_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NULL DEFAULT 0,
  `topic_id` int(11) NULL DEFAULT NULL,
  `topic_class_id` int(11) NULL DEFAULT NULL COMMENT '所属学校类目ID',
  `discuss_list` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `media` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` int(1) NULL DEFAULT 1 COMMENT '帖子类型：1 图文 ，2视频 ，4投票',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址名称',
  `see_num` int(11) NULL DEFAULT 0 COMMENT '浏览量',
  `thumb_num` int(11) NULL DEFAULT 0 COMMENT '点赞数',
  `comment_num` int(11) NULL DEFAULT 0 COMMENT '评论数',
  `longitude` double(10, 6) NULL DEFAULT 0.000000,
  `latitude` double(10, 6) NULL DEFAULT 0.000000,
  `is_bot` int(1) NULL DEFAULT 0,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meow_post_thumb
-- ----------------------------
DROP TABLE IF EXISTS `meow_post_thumb`;
CREATE TABLE `meow_post_thumb`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `post_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meow_topic
-- ----------------------------
DROP TABLE IF EXISTS `meow_topic`;
CREATE TABLE `meow_topic`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NULL DEFAULT 0,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `cover_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `bg_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notice` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '公告',
  `is_user_add_goods` bigint(1) NULL DEFAULT 0 COMMENT '是否允许普通用户发布商品',
  `is_goods` bigint(1) NULL DEFAULT 0 COMMENT '是否开启商品类：0不开启，1开启',
  `goods_class_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '圈子集市' COMMENT '商品类名称',
  `status` int(1) NULL DEFAULT 1 COMMENT '圈子状态：0 关闭 ，1 正常，2 广场显示',
  `user_num` int(11) NULL DEFAULT 1 COMMENT '加入人数',
  `post_num` int(11) NULL DEFAULT 0 COMMENT '帖子数',
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `topic_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '圈子表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for meow_topic_admin
-- ----------------------------
DROP TABLE IF EXISTS `meow_topic_admin`;
CREATE TABLE `meow_topic_admin`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `topic_id`(`topic_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_topic_class
-- ----------------------------
DROP TABLE IF EXISTS `meow_topic_class`;
CREATE TABLE `meow_topic_class`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_topic_top
-- ----------------------------
DROP TABLE IF EXISTS `meow_topic_top`;
CREATE TABLE `meow_topic_top`  (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `topic_id`(`topic_id`, `post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_user
-- ----------------------------
DROP TABLE IF EXISTS `meow_user`;
CREATE TABLE `meow_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sn` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '编号',
  `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `nickname` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户电话',
  `sex` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户性别: [1=男, 2=女]',
  `openid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `unionid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `terminal` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '注册渠道: [1-微信小程序 2-微信公众号 3-手机H5 4-电脑PC 5-苹果APP 6-安卓APP]',
  `is_disable` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否禁用: [0=否, 1=是]',
  `login_ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `post_num` int(11) NULL DEFAULT 0,
  `follow_num` int(11) NULL DEFAULT 0,
  `fans_num` int(11) NULL DEFAULT 0,
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '个人说明',
  `tag_str` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `birthday` date NULL DEFAULT NULL,
  `exp` int(255) NULL DEFAULT 0,
  `integral` int(255) NULL DEFAULT 0,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sn`(`sn`) USING BTREE COMMENT '编号唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_user_session
-- ----------------------------
DROP TABLE IF EXISTS `meow_user_session`;
CREATE TABLE `meow_user_session`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `terminal` tinyint(1) NOT NULL DEFAULT 1 COMMENT '客户端类型：1-微信小程序；2-微信公众号；3-手机H5；4-电脑PC；5-苹果APP；6-安卓APP',
  `token` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '令牌',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `expire_time` int(10) NOT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admin_id_client`(`user_id`, `terminal`) USING BTREE COMMENT '一个用户在一个终端只有一个token',
  UNIQUE INDEX `token`(`token`) USING BTREE COMMENT 'token是唯一的'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户会话表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for meow_user_topic
-- ----------------------------
DROP TABLE IF EXISTS `meow_user_topic`;
CREATE TABLE `meow_user_topic`  (
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `topic_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NULL DEFAULT 0,
  UNIQUE INDEX `uid`(`user_id`, `topic_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
