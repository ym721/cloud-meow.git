<?php return array(
    'root' => array(
        'name' => 'topthink/think',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'f307279918964d817b41d7347f22c481cb445434',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'adbario/php-dot-notation' => array(
            'pretty_version' => '2.5.0',
            'version' => '2.5.0.0',
            'reference' => '081e2cca50c84bfeeea2e3ef9b2c8d206d80ccae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../adbario/php-dot-notation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/aas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/actiontrail' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/adb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/aegis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/afs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/airec' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/alidns' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/alikafka' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/alimt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/aliprobe' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/aliyuncvc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/appmallsservice' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/arms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/arms4finance' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/baas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/batchcompute' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/bss' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/bssopenapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cbn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ccc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ccs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/chatbot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/client' => array(
            'pretty_version' => '1.5.32',
            'version' => '1.5.32.0',
            'reference' => '5bc6f6d660797dcee2c3aef29700ab41ee764f4d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/cloudapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cloudauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cloudesl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cloudmarketing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cloudphoto' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cloudwf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/commondriver' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/companyreg' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/credentials' => array(
            'pretty_version' => '1.1.5',
            'version' => '1.1.5.0',
            'reference' => '1d8383ceef695974a88a3859c42e235fd2e3981a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/credentials',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/crm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/csb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/cusanalyticsconline' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/darabonba-openapi' => array(
            'pretty_version' => '0.2.9',
            'version' => '0.2.9.0',
            'reference' => '4cdfc36615f345786d668dfbaf68d9a301b6dbe2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/darabonba-openapi',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/dataworkspublic' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dbs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dcdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/democenter' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dmsenterprise' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/domain' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/domainintl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/drcloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/drds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dybaseapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dyplsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dypnsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dysmsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/dyvmsapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/eci' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ecs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ecsinc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/edas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ehpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/elasticsearch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/emr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/endpoint-util' => array(
            'pretty_version' => '0.1.1',
            'version' => '0.1.1.0',
            'reference' => 'f3fe88a25d8df4faa3b0ae14ff202a9cc094e6c5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/endpoint-util',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/ess' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/facebody' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/fnf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/foas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ft' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/gateway-spi' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'reference' => '7440f77750c329d8ab252db1d1d967314ccd1fcb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/gateway-spi',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/goodstech' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/gpdb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/green' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/hbase' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/hiknoengine' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/hpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/hsm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/httpdns' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/idst' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/imageaudit' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/imageenhan' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/imagerecog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/imagesearch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/imageseg' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/imm' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/industrybrain' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/iot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/iqa' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/itaas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ivision' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ivpd' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/jaq' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/jarvis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/jarvispublic' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/kms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/linkedmall' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/linkface' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/linkwan' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/live' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/lubancloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/lubanruler' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/market' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/mopen' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/mpserverless' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/mts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/multimediaai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/nas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/netana' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/nlp' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/nlpautoml' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/nlscloudmeta' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/nlsfiletrans' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/objectdet' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ocr' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ocs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/oms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ons' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/onsmqtt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/oos' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/openanalytics' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/openapi-util' => array(
            'pretty_version' => '0.2.1',
            'version' => '0.2.1.0',
            'reference' => 'f31f7bcd835e08ca24b6b8ba33637eb4eceb093a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/openapi-util',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/ossadmin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ots' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/outboundbot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/petadata' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/polardb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/productcatalog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/pts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/push' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/pvtz' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/qualitycheck' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ram' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/rds' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/reid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/retailcloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/rkvstore' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ros' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/rtc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/saf' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/sas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/sasapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/scdn' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/schedulerx2' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/sdk' => array(
            'pretty_version' => '1.8.1796',
            'version' => '1.8.1796.0',
            'reference' => 'd0013183d41d9d6c54574286393474ccc121b8d1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/sdk',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/skyeye' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/slb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/smartag' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/smc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/sms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/smsintl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/snsuapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/sts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/sts-20150401' => array(
            'pretty_version' => '1.1.3',
            'version' => '1.1.3.0',
            'reference' => 'cdbd4ebfe8fe6407cd414cbf88672a0d1395b45b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/sts-20150401',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/taginner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/tea' => array(
            'pretty_version' => '3.2.0',
            'version' => '3.2.0.0',
            'reference' => 'f2bfe50b810f598e1a48e85ee94b7164049d5184',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/tea',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/tea-utils' => array(
            'pretty_version' => '0.2.18',
            'version' => '0.2.18.0',
            'reference' => '103e6cde583cf446b217fbb5c565b9007c238702',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/tea-utils',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/tea-xml' => array(
            'pretty_version' => '0.2.3',
            'version' => '0.2.3.0',
            'reference' => '4bd2303d71c968cb7ae4e487c5fa3023aed3ff3b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/tea-xml',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'alibabacloud/tesladam' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/teslamaxcompute' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/teslastream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ubsms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/ubsmsinner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/uis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/unimkt' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/visionai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/vod' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/voicenavigator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/vpc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/vs' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/wafopenapi' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/welfareinner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/xspace' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/xtrace' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/yqbridge' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'alibabacloud/yundun' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.1796',
            ),
        ),
        'aliyuncs/oss-sdk-php' => array(
            'pretty_version' => 'v2.6.0',
            'version' => '2.6.0.0',
            'reference' => '572d0f8e099e8630ae7139ed3fdedb926c7a760f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aliyuncs/oss-sdk-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'clagiordano/weblibs-configmanager' => array(
            'pretty_version' => 'v1.5.0',
            'version' => '1.5.0.0',
            'reference' => '8802c7396d61a923c9a73e37ead062b24bb1b273',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clagiordano/weblibs-configmanager',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.5.1',
            'version' => '7.5.1.0',
            'reference' => 'b964ca597e86b752cd994f27293e9fa6b6a95ed9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.2',
            'version' => '1.5.2.0',
            'reference' => 'b94b2807d85443f9719887892882d0329d1e2598',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.4.5',
            'version' => '2.4.5.0',
            'reference' => '0454e12ef0cd597ccd2adb036f7bda4e7fface66',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '1.1.10',
            'version' => '1.1.10.0',
            'reference' => '3239285c825c152bcc315fe0e87d6b55f5972ed1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/flysystem-cached-adapter' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem-cached-adapter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'lizhichao/one-sm' => array(
            'pretty_version' => '1.10',
            'version' => '1.10.0.0',
            'reference' => '687a012a44a5bfd4d9143a0234e1060543be455a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lizhichao/one-sm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '2.9.0',
            'version' => '2.9.0.0',
            'reference' => 'e1c0ae1528ce313a450e5e1ad782765c4a8dd3cb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'mtdowling/jmespath.php' => array(
            'pretty_version' => '2.6.1',
            'version' => '2.6.1.0',
            'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mtdowling/jmespath.php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nyholm/psr7' => array(
            'pretty_version' => '1.6.1',
            'version' => '1.6.1.0',
            'reference' => 'e874c8c4286a1e010fb4f385f3a55ac56a05cc93',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nyholm/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nyholm/psr7-server' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => 'b846a689844cef114e8079d8c80f0afd96745ae3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nyholm/psr7-server',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/socialite' => array(
            'pretty_version' => '4.8.0',
            'version' => '4.8.0.0',
            'reference' => 'e55fdf50f8003be8f03a85a7e5a5b7c5716f4c9a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/socialite',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/message-factory' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'reference' => 'a478cb11f66a6ac48d8954216cfed9aa06a501a1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/message-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.1',
            'version' => '1.1.0.0',
            'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0 || 2.0.0 || 3.0.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/simple-cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache' => array(
            'pretty_version' => 'v5.4.22',
            'version' => '5.4.22.0',
            'reference' => '5ed986c4ef65f0dea5e9753630b5cb1f07f847d6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '64be4a7acb83b6f2bf6de9a02cee6dad41277ebc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/cache-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client' => array(
            'pretty_version' => 'v6.0.20',
            'version' => '6.0.20.0',
            'reference' => '541c04560da1875f62c963c3aab6ea12a7314e11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'reference' => '4184b9b63af1edaf35b6a7974c6f1f9f33294129',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '3.0',
            ),
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v6.0.20',
            'version' => '6.0.20.0',
            'reference' => 'e16b2676a4b3b1fa12378a20b29c364feda2a8d6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/mime' => array(
            'pretty_version' => 'v6.0.19',
            'version' => '6.0.19.0',
            'reference' => 'd7052547a0070cbeadd474e172b527a00d657301',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mime',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '639084e360537a19f9ee352433b84ce831f3d2da',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '869329b1e9894268a8a61dabb69153029b7a8c97',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '9e8ecb5f92152187c4799efd3c96b78ccab18ff9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '707403074c8ea6e2edaf8794b0157a0bfa52157a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/psr-http-message-bridge' => array(
            'pretty_version' => 'v2.1.3',
            'version' => '2.1.3.0',
            'reference' => 'd444f85dddf65c7e57c58d8e5b3a4dbb593b1840',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/psr-http-message-bridge',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v4.4.47',
            'version' => '4.4.47.0',
            'reference' => '1069c7a3fca74578022fab6f81643248d02f8e63',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/var-exporter' => array(
            'pretty_version' => 'v6.0.19',
            'version' => '6.0.19.0',
            'reference' => 'df56f53818c2d5d9f683f4ad2e365ba73a3b69d2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-exporter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'reference' => '02c1859112aa779d9ab394ae4f3381911d84052b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'thenorthmemory/xml' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'reference' => '6f50c63450a0b098772423f8bdc3c4ad2c4c30bb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../thenorthmemory/xml',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/framework' => array(
            'pretty_version' => 'v6.1.2',
            'version' => '6.1.2.0',
            'reference' => '67235be5b919aaaf1de5aed9839f65d8e766aca3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'f307279918964d817b41d7347f22c481cb445434',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-filesystem' => array(
            'pretty_version' => 'v1.0.3',
            'version' => '1.0.3.0',
            'reference' => '29f19f140a9267c717fecd7ccb22c84c2d72382e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-filesystem',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-multi-app' => array(
            'pretty_version' => 'v1.0.16',
            'version' => '1.0.16.0',
            'reference' => '07b9183855150455e1f76f8cbe9d77d6d1bc399f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-multi-app',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v2.0.60',
            'version' => '2.0.60.0',
            'reference' => '8bc34a4307fa27186c0e96a9b3de3cb23aa1ed46',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-trace' => array(
            'pretty_version' => 'v1.6',
            'version' => '1.6.0.0',
            'reference' => '136cd5d97e8bdb780e4b5c1637c588ed7ca3e142',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-trace',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'w7corp/easywechat' => array(
            'pretty_version' => '6.11.3',
            'version' => '6.11.3.0',
            'reference' => 'bf84fca683508fbc27c9d57d88674fcddac9fbbe',
            'type' => 'library',
            'install_path' => __DIR__ . '/../w7corp/easywechat',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
