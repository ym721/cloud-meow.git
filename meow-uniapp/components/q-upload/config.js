let baseUrl = '';

if (process.env.NODE_ENV === 'development') {
	// 开发环境
	baseUrl = "http://lite.a.com";
	// baseUrl = "https://lite.meoyun.com";
} else {
	// 生产环境
	baseUrl = "https://lite.meoyun.com";
}

// 图片地址(阿里云OSS地址)
const fileUrl = 'https://lite-meoyun-com.oss-cn-hangzhou.aliyuncs.com';

// 评论订阅消息模板
const replyTmplId = 'RS5-OESt_YwwbgRTYKFPZtB6WFr5S-OfkZop_8pLbok';

export default {
	baseUrl,
	fileUrl,
	replyTmplId,
	domain: baseUrl + "/api/",
}