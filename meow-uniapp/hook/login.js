import http from '../utils/request';
import store from '../store'
/**
 * 微信小程序登录
 */
export const mnpLogin = () => {
	let that = this;
	let userInfo = store.state.userInfo;
	if (!userInfo) {
		wx.login({
			success(res) {
				http.post('login/mnpLogin', {
					code: res.code
				}).then(res => {
					if (res.code === 1) {
						let userInfo = res.result;

						// 判断是否是默认头像和昵称
						const defaultNickName = userInfo.nickname;
						const defaultAvatar = userInfo.avatar;
						if (defaultNickName.includes("用户") || defaultAvatar.includes(
								"resource/avatar")) {
							uni.navigateTo({
								url: "/pages/user/write-info"
							})
						}

						store.commit('login', userInfo);
					}
				});
			}
		})
	}
}