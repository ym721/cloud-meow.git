import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: uni.getStorageSync("hasLogin"),
		userInfo: uni.getStorageSync("userInfo"),
		currentTopicId: uni.getStorageSync("currentTopicId"),
		currentTopic: uni.getStorageSync("currentTopic")
	},
	mutations: {
		login(state, userInfo) {
			state.hasLogin = true;
			state.userInfo = userInfo;

			uni.setStorageSync("userInfo", userInfo);
			uni.setStorageSync("hasLogin", true);
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = null;
			uni.removeStorageSync("hasLogin");
			uni.removeStorageSync("userInfo");

		},
		setCurrentTopicId(state, id) {
			state.currentTopicId = id;
			uni.setStorageSync("currentTopicId", id);
		},
		setCurrentTopic(state, info) {
			state.currentTopic = info;
			uni.setStorageSync("currentTopic", info);
		}
	}
})

export default store