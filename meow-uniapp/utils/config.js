let baseUrl = '';

if (process.env.NODE_ENV === 'development') {
	// 开发环境
	baseUrl = "https://q.meoyun.com";
} else {
	// 生产环境
	baseUrl = "https://q.meoyun.com";
}

// 图片地址(阿里云OSS地址)
const fileUrl = 'https://q-meoyun-com.oss-cn-shenzhen.aliyuncs.com';

// 评论订阅消息模板
const replyTmplId = 'wIrceeMaj2dGafLyZhw4vtlpFFa0UdDw2Obj9ljfm4k';

export default {
	baseUrl,
	fileUrl,
	replyTmplId,
	domain: baseUrl + "/api/",
}