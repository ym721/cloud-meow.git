import config from '@/utils/config.js'

export default {
	imgPath(url) {
		if (!url) return

		if (url.includes('http://') || url.includes('https://')) {
			return url
		} else {
			return config.fileUrl + '/' + url + '?x-oss-process=image/resize,w_500,m_lfit'
		}
	},
	videoPath(url) {
		return config.fileUrl + '/' + url
	},
	videoCover(url) {
		return config.fileUrl + '/' + url + '?x-oss-process=video/snapshot,t_0,f_jpg'
	}
}