import config from './config.js';
import store from '../store/index.js';
export default {
	request(options = {}) {
		return new Promise((resolve, reject) => {
			let url = options.url;
			if (url.indexOf("http://") == -1 && url.indexOf("https://") == -1) {
				options.url = config.domain + url;
			}

			let userInfo = store.state.userInfo
			if (userInfo && userInfo.token) {
				options.header.token = userInfo.token;
			}
			options.complete = (response) => {
				if (response.statusCode == 200) {
					let code = response.data.code;
					let show = response.data.show;

					if (code === 0 && show === 1) {
						setTimeout(() => {
							uni.showToast({
								title: response.data.msg,
								icon: "none"
							}, 500);
						})
					}

					if (code == 420) {
						store.commit("logout");
						uni.navigateTo({
							url: "/pages/user/login"
						})
					}

					resolve(response.data)
				} else {
					uni.showToast({
						title: response.data.msg,
						icon: "none"
					});
				}
			}

			uni.request(options)
		})
	},

	post(url, data = {}, header = {}) {

		let options = {
			url: url,
			data: data,
			header: header,
			method: "POST"
		}

		return this.request(options);
	},

	get(url, data = {}, header = {}) {
		let options = {
			url: url,
			data: data,
			header: header
		}

		return this.request(options);
	}
};